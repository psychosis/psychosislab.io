---
layout: full-width
title: About
---

I've struggled with psychosis, with little to no mental health treatment, for two decades.  I am beginning treatment with a plan to unabashedly tell the psychiatrist everything.  My previous stint with treatment had paranoia and delusions preventing me from giving all the information.  This led to ineffectual treatment.

I am writing this partially as an exercise of motivation to myself and partially to chronicle my experiences.  It will include what I remember of psychotic episodes in the past, current symptoms I have and the ways I have been coping over the years.  This is not a universal picture into psychosis, it is an intimate portrayal of my psychosis.

As of this time, I do not have a diagnosis that includes my psychosis.  Once I have that diagnosis, I will include it.  I am not including any speculative diagnoses out of respect for others who are struggling with mental health.
